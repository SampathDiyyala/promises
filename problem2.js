const fs = require('fs');
const { resolve } = require('path');
const path = require('path');

const lipsumFile = path.resolve('/home/sampath/Downloads/lipsum.txt');
const upperFile = path.resolve(__dirname, './upperCaseFile.txt');
const lowerFile = path.resolve(__dirname, './lowerCaseFile.txt');
const sentences = path.resolve(__dirname, './sentencesFile.txt');
const sortFile = path.resolve(__dirname, './sortedDataFile.txt');
const filenames = path.resolve(__dirname, './filenames.txt');

function readFiles (filepath) {
    return new Promise((resolve, reject) => {
        fs.readFile(filepath, 'utf-8', (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        })
    })
}


function convertToUpperCase (data) {
    return new Promise((resolve, reject) => {
        let uppercase = data.toUpperCase();
        resolve(uppercase);
    })
}

function writeFiles (file, content) {
    fs.writeFile(file, content, (err) => {
        if (err) {
            reject(err);
        } else {
            resolve();
        }
    })

}

function convertToLowerCase (data) {
    return new Promise((resolve, reject) => {
        let lowercase = data.toLowerCase();
        resolve(lowercase);
    })
}

function splitSentences (data) {
    let splitData = data.toString().split(/[!.\n]/);
    let sentence = [];
    Data = splitData.filter(sentence => sentence);
    for (let i = 0; i < Data.length; i++) {
        if (Data[i] === ' ') {
            continue;
        }
        else{
            let str = Data[i].trim();
            sentence.push(str.charAt(0).toUpperCase() + str.substring(1) + '.');
        }
    
    }
    return sentence;
}

function appendDataToFile (filename, data) {
    return new Promise((resolve, reject) => {
        fs.appendFile(filename, data, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        })

    })
}

function sortData (data,filename) {
    let files=data.split('\n');
    for(let i=0;i<files.length;i++){
        readFiles(files[i])
        .then((data) => {
            let content=data.split('\n').sort();
            content.forEach(line => {
                //console.log(line)
                appendDataToFile(filename,`${line}\n`);
            })
            
        })
    }
}


function deleteFiles (data) {
    let filenames = data.split('\n');
    return new Promise((resolve, reject) => {
        for (let i = 0; i < filenames.length; i++) {
            fs.unlink(filenames[i], function (err) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            })
        }
    });
}


readFiles(lipsumFile)
.then((data) => convertToUpperCase(data))
.then((data) => convertToUpperCase(data))
.then((data) => writeFiles(upperFile, data))
.then(() => writeFiles(filenames, upperFile))
.then(() => readFiles(upperFile))
.then((data) => convertToLowerCase(data))
.then((data) => splitSentences(data))
.then((data) => writeFiles(lowerFile, data.toString()))
.then(() => appendDataToFile(filenames, `\n${lowerFile}`))
//.then(() => appendDataToFile(filenames, `\n${sentences}`))
.then(() => readFiles(filenames))
.then((data)=>sortData(data,sortFile))
.then(() => appendDataToFile(filenames, `\n${sortFile}`))
.then(()=>readFiles(filenames))
.then((data) => deleteFiles(data))
.catch((err) => console.log(err))
.finally(()=>{console.log("All the files deleted successfully")})
