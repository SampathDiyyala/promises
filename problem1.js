const fs=require('fs')
const path=require('path')
const dir=__dirname + '/jsonFiles/'
const data="random data"

//create directory

function createDirectory(){
    return new Promise((resolve,reject)=>{
        fs.mkdir(dir,(err)=>{
            if(err){
                reject(err)
            }else{
                resolve()
            }
        })
    })
}

function writeFiles(path,data){
    return new Promise((resolve,reject)=>{
        fs.writeFile(path,data,(err)=>{
            if(err){
                reject(err)
            }else{
                resolve()
            }
        })
    })
}

function deleteFiles(path){
    return new Promise( (resolve,reject)=>{
        fs.readdir(path,(err,data)=>{
            if(err){
                reject(err)
            }else{
                resolve(data.forEach(file => {
                    fs.unlink(dir + file, (err) => {
                    });   
                }))
            }

        })   
    } )
}


createDirectory(dir)
.then(()=>{writeFiles(dir + 'file1.json',data)})
.then(()=>{writeFiles( dir + 'file2.json',data)})
.then(()=>{writeFiles(dir + 'file3.json',data)})
.then(()=>{writeFiles(dir + 'file4.json',data)})
.then(()=>{deleteFiles(dir)})
.catch(err=>console.log(err))
.finally(()=>{console.log("Delete all the json files successfully")})

